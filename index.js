// alert("alo")

// To create a class in JS, we use the class keyword and {}
// naming convention for classes begins with uppercase characters
/*
	Syntax:
		class ClassName {
			properties
		};
*/

class Dog {
	// we add consturctor method to a class to be able to initialize values upon instantiation of an object from a class
	constructor(name, breed, age){

		this.name = name;
		this.breed = breed;
		this.age = age * 7;
	}

}

// instantiation - process of creating objects
// an object created class is called an instance

let dog1 = new Dog("Puchi", "golden retriever", 0.6);
console.log(dog1)

/*
	mini-activity
*/

class Person {
	constructor(name, age, nationality, address){

		this.name = name;
		this.nationality = nationality;
		this.address = address;

		// check typeof age
		if(typeof age === 'number'){
			if (age >= 18) {
				this.age = age;
			} else {
				this.age = undefined
			}
		} else {
			this.age = undefined
		}
	}

	greet(){
		console.log("Hello! Good Morning!")
		return this;
	}

	introduce(){
		console.log(`Hi! My name is ${this.name}`)
		return this;
	}

	changeAddress(address){
		let newAddress = address
		console.log(`${this.name} now has lives in ${newAddress}`)
		return this;

	}

}

let person1 = new Person ("Tony", 30, "Australian ", "Australia" )
let person2 = new Person ("Uzikiel", "27", "Brazilian  ", "Brazil" )
let person3 = new Person ("Bale", 17, "Ugandan  ", "Uganda" )

console.log(person1)
console.log(person2)
console.log(person3)

/*Activity #3:
     1. Should class methods be included in the class constructor?
        Answer: No


    2. Can class methods be separated by commas?
        Answer: No


    3. Can we update an object’s properties via dot notation?
        Answer: Yes


    4. What do you call the methods used to regulate access to an object’s properties?
        Answer: dot(.)notation


    5. What does a method need to return in order for it to be chainable?
        Answer: return this;

*/

// Function Coding Activity


/*
	Mini-Quiz:
	1. What is the blueprint where objects are created from?
        Answer: Class
	
	2. What is the naming convention applied to classes?
        Answer: Begins with Uppercase letters

	3. What keyword do we use to create objects from a class?
        Answer: new
	
	4. What is the technical term for creating an object from a class?
        Answer: Instantiation
	
	5. What class method dictates HOW objects will be created from that class?
		Answer: Constuctor
*/

class Student {
	constructor(name, email, grades){

		this.name = name;
		this.email = email;

		// check if first the array has 4 elements
		if(grades.length === 4){
			if(grades.every(grade => grade >= 0 && grade <= 100)){
				this.grades = grades;
			} else {
				this.grades = undefined;
			}
		} else {
			this.grades = undefined;
		}

		this.gradeAve = undefined;
		this.passed = undefined;
		this.pasedWithHonnors = undefined;
	}

	// class methods - would be common to all instances.
	// make sure they are NOT separated by a comma
	login(){
		console.log(`${this.email} has logged in`)
		return this;
	}

	logout(email){
    	console.log(`${this.email} has logged out`);
    	return this;
	}

	listGrades(grades){
    	this.grades.forEach(grade => {
        	console.log(`${this.name}'s quarterly grade averages are: ${grade}`);
    	})
    	return this;
	}

	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade)
		this.gradeAve = sum / 4;
		return this;
	}

	willPass(){
		this.passed = this.computeAve().gradeAve >= 85 ? true : false
		return this;
				
	}

	willPassWithHonors(){
		if(this.passed) {
			if(this.gradeAve >= 90){
				this.pasedWithHonnors = true
			} else {
				this.pasedWithHonnors = false
			}
		} else {
			this.pasedWithHonnors = false
		}
		return this;
	}
}

// instantiate all four students from s2 using Student Class
let studentOne = new Student ("Tony", "starkindustries@mail.com", [89, 84, 78, 88]);
let studentTwo = new Student ("Peter", "spideyman@mail.com", [79, 82, 79, 85]);
let studentThree = new Student ("Wanda", "scarlettMaximoff@mail.com", [87, 89, 91, 93]);
let studentFour = new Student ("Steve", "captainRogers@mail.com", [91, 89, 92, 93]);

console.log(studentOne)
console.log(studentTwo)
console.log(studentThree)
console.log(studentFour)

